Mika Keyboard
=============

Extra USB-keyboard for computer, based on Raspberry Pi Pico and CircuitPython.

PCB design is done using KiCad 5.10.

![](pictures/mika-keyboard.png)

![](pictures/mika-keyboard_back.png)


Keyboard Shortcuts
------------------

Keyboard shortcuts are read from the file *shortcuts.txt*. Key names used (for example ALT) are listed in [https://circuitpython.readthedocs.io/projects/hid/en/latest/api.html#adafruit-hid-keycode-keycode](https://circuitpython.readthedocs.io/projects/hid/en/latest/api.html#adafruit-hid-keycode-keycode). The names are from the US keyboard layout!



3D Models
---------

Cherry MX Switch 3D model:
[https://github.com/ConstantinoSchillebeeckx/cherry-mx-switch](https://github.com/ConstantinoSchillebeeckx/cherry-mx-switch)



Debouncers
----------


### Button Debouncer

10kOhm + 10kOhm + 0.56uF

tau_rise = 20 kOhm * 0.56uF = 11.2ms

tau_fall = 10 kOhm * 0.56uF = 5.6ms

With diode added:

tau_rise = 10 kOhm * 0.56uF = 5.6ms

tau_fall = 10 kOhm * 0.56uF = 5.6ms

Note: Cherry MX switch debounce time from the datasheet is 5ms.


### Rotary Encoder Debouncer

These values are from the rotary encoder datasheet.

10kOhm + 10kOhm + 0.1uF

tau_rise = 20 kOhm * 0.1uF = 2ms

tau_fall = 10 kOhm * 0.1uF = 1ms



Thank You
---------

**KiPart** script, create schematic symbols from CSV files. [https://devbisme.github.io/KiPart/docs/_build/singlehtml/index.html](https://devbisme.github.io/KiPart/docs/_build/singlehtml/index.html)

**Replicate Layout** plugin, place and route the layout from a schematic hierarchical sheet only once, and copy the arrangement to all of them. [https://github.com/MitjaNemec/Kicad_action_plugins](https://github.com/MitjaNemec/Kicad_action_plugins)

**Designing an RC debounce circuit** blog post. [https://mayaposch.wordpress.com/2018/06/26/designing-an-rc-debounce-circuit/](https://mayaposch.wordpress.com/2018/06/26/designing-an-rc-debounce-circuit/)
