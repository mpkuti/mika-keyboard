#!/bin/bash

LIB_FOOTER="#End Library"

kipart -s row --overwrite raspberry-pi-pico.csv --output mika-keyboard.lib
kipart -s row --append display.csv --output mika-keyboard.lib
kipart -s row --append rotary-encoder.csv --output mika-keyboard.lib
kipart -s row --append hex-schmitt-trigger.csv --output mika-keyboard.lib

echo $LIB_FOOTER >> ./mika-keyboard.lib
