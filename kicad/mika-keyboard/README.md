## KIFIELD

  $ pyenv local 3.9.7

  $ pip install kifield

  $ kifield --recurse --extract mika-keyboard.sch --insert mika-keyboard.xlsx

  $ kifield --recurse --extract mika-keyboard.xlsx --insert mika-keyboard.sch

## KiBot

Set up the Python environment:

  pyenv local 3.9.7

Install Kicad Automation Scripts, KiBoM:

  pip install kiauto
  pip install kibom
  pip install pcbdraw
  pip install --no-compile kibot

Run KiBot

  kibot -e mika-keyboard.sch
  
  kibot --schematic mika-keyboard.sch --board-file mika-keyboard.kicad_pcb --plot-config mika-keyboard.kibot.yaml
