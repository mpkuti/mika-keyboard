EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 13
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text Label 3050 1550 0    50   ~ 0
GND
Text Label 3050 2050 0    50   ~ 0
GND
Text Label 3050 2550 0    50   ~ 0
GND
Text Label 3050 3050 0    50   ~ 0
GND
NoConn ~ 3050 1650
NoConn ~ 3050 1850
Text Label 1250 1550 2    50   ~ 0
GND
Text Label 1250 2050 2    50   ~ 0
GND
Text Label 1250 2550 2    50   ~ 0
GND
Text Label 1250 3050 2    50   ~ 0
GND
Text Label 2150 3950 3    50   ~ 0
GND
NoConn ~ 2050 3950
NoConn ~ 2250 3950
Text Label 3050 1750 0    50   ~ 0
3V3
$Sheet
S 6150 1400 800  400 
U 61620D37
F0 "Rotary Knob 1" 50
F1 "rotary-knob.sch" 50
F2 "GND" I L 6150 1600 50 
F3 "3V3" I L 6150 1500 50 
F4 "ROT_A" O R 6950 1600 50 
F5 "ROT_B" O R 6950 1700 50 
F6 "BUTTON_CAP" O R 6950 1500 50 
$EndSheet
$Sheet
S 7800 1400 800  400 
U 61628A1B
F0 "Rotary Knob 2" 50
F1 "rotary-knob.sch" 50
F2 "GND" I L 7800 1600 50 
F3 "3V3" I L 7800 1500 50 
F4 "ROT_A" O R 8600 1600 50 
F5 "ROT_B" O R 8600 1700 50 
F6 "BUTTON_CAP" O R 8600 1500 50 
$EndSheet
$Sheet
S 9500 1400 800  400 
U 61628BB6
F0 "Rotary Knob 3" 50
F1 "rotary-knob.sch" 50
F2 "GND" I L 9500 1600 50 
F3 "3V3" I L 9500 1500 50 
F4 "ROT_A" O R 10300 1600 50 
F5 "ROT_B" O R 10300 1700 50 
F6 "BUTTON_CAP" O R 10300 1500 50 
$EndSheet
Text Label 6150 1600 2    50   ~ 0
GND
Text Label 7800 1600 2    50   ~ 0
GND
Text Label 9500 1600 2    50   ~ 0
GND
Text Label 6150 1500 2    50   ~ 0
3V3
Text Label 7800 1500 2    50   ~ 0
3V3
Text Label 9500 1500 2    50   ~ 0
3V3
Text Label 1250 2250 2    50   ~ 0
ROT3_BUTTON
Text Label 6950 1500 0    50   ~ 0
ROT1_BUTTON
Text Label 8600 1500 0    50   ~ 0
ROT2_BUTTON
Text Label 10300 1500 0    50   ~ 0
ROT3_BUTTON
Text Label 6950 1600 0    50   ~ 0
ROT1_A
Text Label 6950 1700 0    50   ~ 0
ROT1_B
Text Label 8600 1600 0    50   ~ 0
ROT2_A
Text Label 8600 1700 0    50   ~ 0
ROT2_B
Text Label 10300 1600 0    50   ~ 0
ROT3_A
Text Label 10300 1700 0    50   ~ 0
ROT3_B
Text Label 1250 2150 2    50   ~ 0
ROT2_B
Text Label 1250 1450 2    50   ~ 0
ROT3_A
Text Label 1250 1650 2    50   ~ 0
ROT3_B
$Comp
L power:PWR_FLAG #FLG01
U 1 1 616482C0
P 800 1550
F 0 "#FLG01" H 800 1625 50  0001 C CNN
F 1 "PWR_FLAG" H 800 1723 50  0000 C CNN
F 2 "" H 800 1550 50  0001 C CNN
F 3 "~" H 800 1550 50  0001 C CNN
	1    800  1550
	1    0    0    -1  
$EndComp
Wire Wire Line
	1250 1550 800  1550
Text Label 1250 2750 2    50   ~ 0
BUTTON1
Text Label 1250 2450 2    50   ~ 0
BUTTON2
Text Label 1250 1750 2    50   ~ 0
BUTTON3
Text Label 3050 2750 0    50   ~ 0
BUTTON4
Text Label 1250 2350 2    50   ~ 0
BUTTON5
Text Label 1250 1850 2    50   ~ 0
BUTTON6
Text Label 1250 3250 2    50   ~ 0
BUTTON7
Text Label 3050 2650 0    50   ~ 0
BUTTON8
Text Label 1250 1350 2    50   ~ 0
BUTTON9
Text Label 1250 2950 2    50   ~ 0
ROT1_A
Text Label 1250 3150 2    50   ~ 0
ROT1_B
NoConn ~ 3050 1350
NoConn ~ 3050 1450
Text Label 1250 2650 2    50   ~ 0
ROT2_BUTTON
$Sheet
S 6150 2500 800  400 
U 61696A39
F0 "Push Button 1" 50
F1 "button.sch" 50
F2 "GND" I L 6150 2700 50 
F3 "3V3" I L 6150 2600 50 
F4 "BUTTON" O R 6950 2600 50 
$EndSheet
Text Label 6150 2600 2    50   ~ 0
3V3
Text Label 6150 2700 2    50   ~ 0
GND
Text Label 6950 2600 0    50   ~ 0
BUTTON1
$Sheet
S 7850 2500 800  400 
U 6169E83C
F0 "Push Button 2" 50
F1 "button.sch" 50
F2 "GND" I L 7850 2700 50 
F3 "3V3" I L 7850 2600 50 
F4 "BUTTON" O R 8650 2600 50 
$EndSheet
Text Label 7850 2600 2    50   ~ 0
3V3
Text Label 7850 2700 2    50   ~ 0
GND
Text Label 8650 2600 0    50   ~ 0
BUTTON2
$Sheet
S 9500 2500 800  400 
U 6169EA04
F0 "Push Button 3" 50
F1 "button.sch" 50
F2 "GND" I L 9500 2700 50 
F3 "3V3" I L 9500 2600 50 
F4 "BUTTON" O R 10300 2600 50 
$EndSheet
Text Label 9500 2600 2    50   ~ 0
3V3
Text Label 9500 2700 2    50   ~ 0
GND
Text Label 10300 2600 0    50   ~ 0
BUTTON3
$Sheet
S 6150 3300 800  400 
U 6169EEAD
F0 "Push Button 4" 50
F1 "button.sch" 50
F2 "GND" I L 6150 3500 50 
F3 "3V3" I L 6150 3400 50 
F4 "BUTTON" O R 6950 3400 50 
$EndSheet
Text Label 6150 3400 2    50   ~ 0
3V3
Text Label 6150 3500 2    50   ~ 0
GND
Text Label 6950 3400 0    50   ~ 0
BUTTON4
$Sheet
S 7850 3300 800  400 
U 6169EEB5
F0 "Push Button 5" 50
F1 "button.sch" 50
F2 "GND" I L 7850 3500 50 
F3 "3V3" I L 7850 3400 50 
F4 "BUTTON" O R 8650 3400 50 
$EndSheet
Text Label 7850 3400 2    50   ~ 0
3V3
Text Label 7850 3500 2    50   ~ 0
GND
Text Label 8650 3400 0    50   ~ 0
BUTTON5
$Sheet
S 9500 3300 800  400 
U 6169EEBD
F0 "Push Button 6" 50
F1 "button.sch" 50
F2 "GND" I L 9500 3500 50 
F3 "3V3" I L 9500 3400 50 
F4 "BUTTON" O R 10300 3400 50 
$EndSheet
Text Label 9500 3400 2    50   ~ 0
3V3
Text Label 9500 3500 2    50   ~ 0
GND
Text Label 10300 3400 0    50   ~ 0
BUTTON6
$Sheet
S 6150 4100 800  400 
U 6169F699
F0 "Push Button 7" 50
F1 "button.sch" 50
F2 "GND" I L 6150 4300 50 
F3 "3V3" I L 6150 4200 50 
F4 "BUTTON" O R 6950 4200 50 
$EndSheet
Text Label 6150 4200 2    50   ~ 0
3V3
Text Label 6150 4300 2    50   ~ 0
GND
Text Label 6950 4200 0    50   ~ 0
BUTTON7
$Sheet
S 7850 4100 800  400 
U 6169F6A1
F0 "Push Button 8" 50
F1 "button.sch" 50
F2 "GND" I L 7850 4300 50 
F3 "3V3" I L 7850 4200 50 
F4 "BUTTON" O R 8650 4200 50 
$EndSheet
Text Label 7850 4200 2    50   ~ 0
3V3
Text Label 7850 4300 2    50   ~ 0
GND
Text Label 8650 4200 0    50   ~ 0
BUTTON8
$Sheet
S 9500 4100 800  400 
U 6169F6A9
F0 "Push Button 9" 50
F1 "button.sch" 50
F2 "GND" I L 9500 4300 50 
F3 "3V3" I L 9500 4200 50 
F4 "BUTTON" O R 10300 4200 50 
$EndSheet
Text Label 9500 4200 2    50   ~ 0
3V3
Text Label 9500 4300 2    50   ~ 0
GND
Text Label 10300 4200 0    50   ~ 0
BUTTON9
NoConn ~ 3050 2850
$Comp
L mika-keyboard:Raspberry_Pi_Pico U1
U 1 1 6167D044
P 1250 1350
F 0 "U1" H 2150 1637 60  0000 C CNN
F 1 "Raspberry_Pi_Pico" H 2150 1531 60  0000 C CNN
F 2 "mika-keyboard:Raspberry_Pi_Pico_SMD" H 1450 1400 60  0001 L CNN
F 3 "https://datasheets.raspberrypi.org/pico/pico-datasheet.pdf" H 1450 1200 60  0001 L CNN
F 4 "RASPBERRY PI PICO RP2040 BOARD" H 1250 1350 50  0001 C CNN "desc"
F 5 "2648-SC0915CT-ND" H 1250 1350 50  0001 C CNN "digikey#"
F 6 "Raspberry Pi" H 1250 1350 50  0001 C CNN "manf"
F 7 "SC0915" H 1250 1350 50  0001 C CNN "manf#"
F 8 "https://www.digikey.com/en/products/detail/raspberry-pi/SC0915/13624793?s=N4IgTCBcDaIMoGEAMBOAjAVhAXQL5A" H 1250 1350 50  0001 C CNN "PurchaseLink"
F 9 "SMD" H 1250 1350 50  0001 C CNN "Mount Type"
F 10 "custom" H 1250 1350 50  0001 C CNN "Package"
	1    1250 1350
	1    0    0    -1  
$EndComp
NoConn ~ 3050 2450
$Comp
L Mechanical:MountingHole_Pad H1
U 1 1 6172BFFD
P 650 5950
F 0 "H1" H 750 5999 50  0001 L CNN
F 1 "MountingHole_Pad" H 750 5908 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_Pad" H 650 5950 50  0001 C CNN
F 3 "~" H 650 5950 50  0001 C CNN
	1    650  5950
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR01
U 1 1 6172DCE4
P 650 6050
F 0 "#PWR01" H 650 5800 50  0001 C CNN
F 1 "GND" H 655 5877 50  0000 C CNN
F 2 "" H 650 6050 50  0001 C CNN
F 3 "" H 650 6050 50  0001 C CNN
	1    650  6050
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H2
U 1 1 6172FC15
P 650 6450
F 0 "H2" H 750 6499 50  0001 L CNN
F 1 "MountingHole_Pad" H 750 6408 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_Pad" H 650 6450 50  0001 C CNN
F 3 "~" H 650 6450 50  0001 C CNN
	1    650  6450
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR02
U 1 1 6172FC1B
P 650 6550
F 0 "#PWR02" H 650 6300 50  0001 C CNN
F 1 "GND" H 655 6377 50  0000 C CNN
F 2 "" H 650 6550 50  0001 C CNN
F 3 "" H 650 6550 50  0001 C CNN
	1    650  6550
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H3
U 1 1 61730BC9
P 650 6950
F 0 "H3" H 750 6999 50  0001 L CNN
F 1 "MountingHole_Pad" H 750 6908 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_Pad" H 650 6950 50  0001 C CNN
F 3 "~" H 650 6950 50  0001 C CNN
	1    650  6950
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR03
U 1 1 61730BCF
P 650 7050
F 0 "#PWR03" H 650 6800 50  0001 C CNN
F 1 "GND" H 655 6877 50  0000 C CNN
F 2 "" H 650 7050 50  0001 C CNN
F 3 "" H 650 7050 50  0001 C CNN
	1    650  7050
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H4
U 1 1 61731B9B
P 650 7450
F 0 "H4" H 750 7499 50  0001 L CNN
F 1 "MountingHole_Pad" H 750 7408 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_Pad" H 650 7450 50  0001 C CNN
F 3 "~" H 650 7450 50  0001 C CNN
	1    650  7450
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR04
U 1 1 61731BA1
P 650 7550
F 0 "#PWR04" H 650 7300 50  0001 C CNN
F 1 "GND" H 655 7377 50  0000 C CNN
F 2 "" H 650 7550 50  0001 C CNN
F 3 "" H 650 7550 50  0001 C CNN
	1    650  7550
	1    0    0    -1  
$EndComp
$Comp
L mika-keyboard:DFR0647 DS1
U 1 1 61713242
P 4900 1200
F 0 "DS1" H 5428 1103 60  0000 L CNN
F 1 "DFR0647" H 5428 997 60  0000 L CNN
F 2 "mika-keyboard:DFR0647" H 5100 1250 60  0001 L CNN
F 3 "https://wiki.dfrobot.com/Monochrome%200.91%E2%80%9D128x32%20I2C%20OLED%20Display%20Chip%20Pad%20SKU:%20DFR0647" H 5100 1050 60  0001 L CNN
F 4 "DFR0647" H 5100 1150 60  0001 L CNN "manf#"
F 5 "MONOCHROME 0.91 128X32 I2C OLED" H 5250 1350 60  0000 C BNN "desc"
F 6 "1738-DFR0647-ND" H 4900 1200 50  0001 C CNN "digikey#"
F 7 "DFRobot" H 4900 1200 50  0001 C CNN "manf"
F 8 "https://www.digikey.com/en/products/detail/dfrobot/DFR0647/12324928?s=N4IgTCBcDaIIwHYDMAOAtAEQGICUAMAbACwJoByGIAugL5A" H 4900 1200 50  0001 C CNN "PurchaseLink"
F 9 "SMD" H 4900 1200 50  0001 C CNN "Mount Type"
F 10 "custom" H 4900 1200 50  0001 C CNN "Package"
F 11 "NOT ASSEMBLED!" H 4900 1200 50  0001 C CNN "NOTE"
	1    4900 1200
	1    0    0    -1  
$EndComp
Text Label 4900 1200 2    50   ~ 0
3V3
Text Label 4900 1300 2    50   ~ 0
GND
Text Label 4900 1400 2    50   ~ 0
SCL
Text Label 4900 1500 2    50   ~ 0
SDA
$Comp
L pspice:CAP C2
U 1 1 617150B9
P 4400 1450
F 0 "C2" H 4578 1496 50  0000 L CNN
F 1 "0.1u" H 4578 1405 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 4400 1450 50  0001 C CNN
F 3 "https://www.yageo.com/upload/media/product/productsearch/datasheet/mlcc/UPY-GPHC_X7R_6.3V-to-50V_20.pdf" H 4400 1450 50  0001 C CNN
F 4 "CAP CER 0.1UF 50V X7R 0805" H 4400 1450 50  0001 C CNN "desc"
F 5 "311-1140-1-ND" H 4400 1450 50  0001 C CNN "digikey#"
F 6 "CC0805KRX7R9BB104" H 4400 1450 50  0001 C CNN "manf#"
F 7 "YAGEO" H 4400 1450 50  0001 C CNN "manf"
F 8 "302010165" H 4400 1450 50  0001 C CNN "SeeedOPL#"
F 9 "302010165" H 4400 1450 50  0001 C CNN "SeeedSKU"
F 10 "https://www.digikey.com/en/products/detail/yageo/CC0805KRX7R9BB104/302874?s=N4IgTCBcDaIMwEYEFokBYAMrkDkAiIAugL5A" H 4400 1450 50  0001 C CNN "PurchaseLink"
F 11 "0805" H 4400 1450 50  0001 C CNN "Package"
F 12 "SMD" H 4400 1450 50  0001 C CNN "Mount Type"
	1    4400 1450
	1    0    0    -1  
$EndComp
Text Label 4400 1700 3    50   ~ 0
GND
Wire Wire Line
	4400 1200 4900 1200
$Comp
L pspice:CAP C?
U 1 1 61717956
P 3750 1450
AR Path="/61620D37/61717956" Ref="C?"  Part="1" 
AR Path="/61628A1B/61717956" Ref="C?"  Part="1" 
AR Path="/61628BB6/61717956" Ref="C?"  Part="1" 
AR Path="/61717956" Ref="C1"  Part="1" 
F 0 "C1" H 3928 1496 50  0000 L CNN
F 1 "0.56u" H 3928 1405 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 3750 1450 50  0001 C CNN
F 3 "https://www.yageo.com/upload/media/product/productsearch/datasheet/mlcc/UPY-GPHC_X7R_6.3V-to-50V_20.pdf" H 3750 1450 50  0001 C CNN
F 4 "CAP CER 0.56UF 25V X7R 0805" H 3750 1450 50  0001 C CNN "desc"
F 5 "13-CC0805KKX7R8BB564TR-ND" H 3750 1450 50  0001 C CNN "digikey#"
F 6 "CC0805KKX7R8BB564" H 3750 1450 50  0001 C CNN "manf#"
F 7 "YAGEO" H 3750 1450 50  0001 C CNN "manf"
F 8 "https://www.digikey.com/en/products/detail/yageo/CC0805KKX7R8BB564/11492300?s=N4IgTCBcDaIIwGYC0BhFAGAHOgrAaTwA0B2AJUwCEKcA2AFgBVSkA5AERAF0BfIA" H 3750 1450 50  0001 C CNN "PurchaseLink"
F 9 "0805" H 3750 1450 50  0001 C CNN "Package"
F 10 "SMD" H 3750 1450 50  0001 C CNN "Mount Type"
	1    3750 1450
	1    0    0    -1  
$EndComp
Text Label 3750 1700 3    50   ~ 0
GND
Wire Wire Line
	4400 1200 3750 1200
Connection ~ 4400 1200
Text Label 3050 2150 0    50   ~ 0
SCL
Text Label 3050 2250 0    50   ~ 0
SDA
NoConn ~ 3050 2350
NoConn ~ 3050 3150
NoConn ~ 3050 3250
NoConn ~ 3050 1950
NoConn ~ 3250 3700
NoConn ~ 3050 2950
$Comp
L Mechanical:Fiducial FID1
U 1 1 61785C3B
P 1800 7000
F 0 "FID1" H 1885 7046 50  0000 L CNN
F 1 "Fiducial" H 1885 6955 50  0000 L CNN
F 2 "Fiducial:Fiducial_1mm_Mask2mm" H 1800 7000 50  0001 C CNN
F 3 "~" H 1800 7000 50  0001 C CNN
	1    1800 7000
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:Fiducial FID3
U 1 1 61786565
P 1800 7250
F 0 "FID3" H 1885 7296 50  0000 L CNN
F 1 "Fiducial" H 1885 7205 50  0000 L CNN
F 2 "Fiducial:Fiducial_1mm_Mask2mm" H 1800 7250 50  0001 C CNN
F 3 "~" H 1800 7250 50  0001 C CNN
	1    1800 7250
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:Fiducial FID2
U 1 1 617868AE
P 2400 7000
F 0 "FID2" H 2485 7046 50  0000 L CNN
F 1 "Fiducial" H 2485 6955 50  0000 L CNN
F 2 "Fiducial:Fiducial_1mm_Mask2mm" H 2400 7000 50  0001 C CNN
F 3 "~" H 2400 7000 50  0001 C CNN
	1    2400 7000
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:Fiducial FID4
U 1 1 61786AE9
P 2400 7250
F 0 "FID4" H 2485 7296 50  0000 L CNN
F 1 "Fiducial" H 2485 7205 50  0000 L CNN
F 2 "Fiducial:Fiducial_1mm_Mask2mm" H 2400 7250 50  0001 C CNN
F 3 "~" H 2400 7250 50  0001 C CNN
	1    2400 7250
	1    0    0    -1  
$EndComp
Text Label 1250 1950 2    50   ~ 0
ROT2_A
Text Label 1250 2850 2    50   ~ 0
ROT1_BUTTON
$EndSCHEMATC
