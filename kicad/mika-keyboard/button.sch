EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 5 13
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Switch:SW_Push SW4
U 1 1 61697B5C
P 4000 3700
AR Path="/61696A39/61697B5C" Ref="SW4"  Part="1" 
AR Path="/6169E83C/61697B5C" Ref="SW5"  Part="1" 
AR Path="/6169EA04/61697B5C" Ref="SW6"  Part="1" 
AR Path="/6169EEAD/61697B5C" Ref="SW7"  Part="1" 
AR Path="/6169EEB5/61697B5C" Ref="SW8"  Part="1" 
AR Path="/6169EEBD/61697B5C" Ref="SW9"  Part="1" 
AR Path="/6169F699/61697B5C" Ref="SW10"  Part="1" 
AR Path="/6169F6A1/61697B5C" Ref="SW11"  Part="1" 
AR Path="/6169F6A9/61697B5C" Ref="SW12"  Part="1" 
F 0 "SW4" V 4046 3652 50  0000 R CNN
F 1 "SW_Push" V 3955 3652 50  0000 R CNN
F 2 "mika-keyboard:SW_Cherry_MX_1.00u_PCB" H 4000 3900 50  0001 C CNN
F 3 "https://media.digikey.com/pdf/Data%20Sheets/Cherry%20PDFs/MX%20Series.pdf" H 4000 3900 50  0001 C CNN
F 4 "SWITCH PUSH SPST-NO 0.01A 12V" H 4000 3700 50  0001 C CNN "desc"
F 5 "CH197-ND" H 4000 3700 50  0001 C CNN "digikey#"
F 6 "MX1A-E1NW" H 4000 3700 50  0001 C CNN "manf#"
F 7 "Cherry Americas LLC" H 4000 3700 50  0001 C CNN "manf"
F 8 "https://www.digikey.com/en/products/detail/cherry-americas-llc/MX1A-E1NW/20180?s=N4IgTCBcDaIMIAkCMBOA7AWgHIBEQF0BfIA" V 4000 3700 50  0001 C CNN "PurchaseLink"
F 9 "thru-hole" H 4000 3700 50  0001 C CNN "Mount Type"
F 10 "custom" H 4000 3700 50  0001 C CNN "Package"
	1    4000 3700
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R19
U 1 1 616985F7
P 4000 2350
AR Path="/61696A39/616985F7" Ref="R19"  Part="1" 
AR Path="/6169E83C/616985F7" Ref="R21"  Part="1" 
AR Path="/6169EA04/616985F7" Ref="R23"  Part="1" 
AR Path="/6169EEAD/616985F7" Ref="R25"  Part="1" 
AR Path="/6169EEB5/616985F7" Ref="R27"  Part="1" 
AR Path="/6169EEBD/616985F7" Ref="R29"  Part="1" 
AR Path="/6169F699/616985F7" Ref="R31"  Part="1" 
AR Path="/6169F6A1/616985F7" Ref="R33"  Part="1" 
AR Path="/6169F6A9/616985F7" Ref="R35"  Part="1" 
F 0 "R19" H 4070 2396 50  0000 L CNN
F 1 "10k" H 4070 2305 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 3930 2350 50  0001 C CNN
F 3 "https://www.yageo.com/upload/media/product/productsearch/datasheet/rchip/PYu-RC_Group_51_RoHS_L_11.pdf" H 4000 2350 50  0001 C CNN
F 4 "RC0805FR-0710KL" H 4000 2350 50  0001 C CNN "manf#"
F 5 "311-10.0KCRDKR-ND" H 4000 2350 50  0001 C CNN "digikey#"
F 6 "RES 10K OHM 1% 1/8W 0805" H 4000 2350 50  0001 C CNN "desc"
F 7 "YAGEO" H 4000 2350 50  0001 C CNN "manf"
F 8 "301010361" H 4000 2350 50  0001 C CNN "SeeedOPL#"
F 9 "301010361" H 4000 2350 50  0001 C CNN "SeeedSKU"
F 10 "https://www.digikey.com/en/products/detail/yageo/RC0805FR-0710KL/727535?s=N4IgTCBcDaIMwEYEFoEAYB0aDSBhASgCLb7IByhIAugL5A" H 4000 2350 50  0001 C CNN "PurchaseLink"
F 11 "0805" H 4000 2350 50  0001 C CNN "Package"
F 12 "SMD" H 4000 2350 50  0001 C CNN "Mount Type"
	1    4000 2350
	1    0    0    -1  
$EndComp
$Comp
L pspice:CAP C12
U 1 1 61699C73
P 5500 3750
AR Path="/61696A39/61699C73" Ref="C12"  Part="1" 
AR Path="/6169E83C/61699C73" Ref="C13"  Part="1" 
AR Path="/6169EA04/61699C73" Ref="C14"  Part="1" 
AR Path="/6169EEAD/61699C73" Ref="C15"  Part="1" 
AR Path="/6169EEB5/61699C73" Ref="C16"  Part="1" 
AR Path="/6169EEBD/61699C73" Ref="C17"  Part="1" 
AR Path="/6169F699/61699C73" Ref="C18"  Part="1" 
AR Path="/6169F6A1/61699C73" Ref="C19"  Part="1" 
AR Path="/6169F6A9/61699C73" Ref="C20"  Part="1" 
F 0 "C12" H 5678 3796 50  0000 L CNN
F 1 "0.56u" H 5678 3705 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 5500 3750 50  0001 C CNN
F 3 "https://www.yageo.com/upload/media/product/productsearch/datasheet/mlcc/UPY-GPHC_X7R_6.3V-to-50V_20.pdf" H 5500 3750 50  0001 C CNN
F 4 "CAP CER 0.56UF 25V X7R 0805" H 5500 3750 50  0001 C CNN "desc"
F 5 "13-CC0805KKX7R8BB564TR-ND" H 5500 3750 50  0001 C CNN "digikey#"
F 6 "CC0805KKX7R8BB564" H 5500 3750 50  0001 C CNN "manf#"
F 7 "YAGEO" H 5500 3750 50  0001 C CNN "manf"
F 8 "https://www.digikey.com/en/products/detail/yageo/CC0805KKX7R8BB564/11492300?s=N4IgTCBcDaIIwGYC0BhFAGAHOgrAaTwA0B2AJUwCEKcA2AFgBVSkA5AERAF0BfIA" H 5500 3750 50  0001 C CNN "PurchaseLink"
F 9 "0805" H 5500 3750 50  0001 C CNN "Package"
F 10 "SMD" H 5500 3750 50  0001 C CNN "Mount Type"
	1    5500 3750
	1    0    0    -1  
$EndComp
Wire Wire Line
	4000 2500 4000 2550
Wire Wire Line
	4500 3000 4000 3000
Connection ~ 4000 3000
Wire Wire Line
	4000 3000 4000 3500
Wire Wire Line
	4800 3000 5500 3000
Wire Wire Line
	5500 3000 5500 3500
Text Label 5500 4000 3    50   ~ 0
GND
Text HLabel 4000 3900 3    50   Input ~ 0
GND
Text HLabel 4000 2200 1    50   Input ~ 0
3V3
Text HLabel 6000 3000 2    50   Output ~ 0
BUTTON
Wire Wire Line
	6000 3000 5500 3000
Connection ~ 5500 3000
$Comp
L Device:R R20
U 1 1 6169C5C6
P 4650 3000
AR Path="/61696A39/6169C5C6" Ref="R20"  Part="1" 
AR Path="/6169E83C/6169C5C6" Ref="R22"  Part="1" 
AR Path="/6169EA04/6169C5C6" Ref="R24"  Part="1" 
AR Path="/6169EEAD/6169C5C6" Ref="R26"  Part="1" 
AR Path="/6169EEB5/6169C5C6" Ref="R28"  Part="1" 
AR Path="/6169EEBD/6169C5C6" Ref="R30"  Part="1" 
AR Path="/6169F699/6169C5C6" Ref="R32"  Part="1" 
AR Path="/6169F6A1/6169C5C6" Ref="R34"  Part="1" 
AR Path="/6169F6A9/6169C5C6" Ref="R36"  Part="1" 
F 0 "R20" H 4720 3046 50  0000 L CNN
F 1 "10k" H 4720 2955 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 4580 3000 50  0001 C CNN
F 3 "https://www.yageo.com/upload/media/product/productsearch/datasheet/rchip/PYu-RC_Group_51_RoHS_L_11.pdf" H 4650 3000 50  0001 C CNN
F 4 "RES 10K OHM 1% 1/8W 0805" H 4650 3000 50  0001 C CNN "desc"
F 5 "311-10.0KCRDKR-ND" H 4650 3000 50  0001 C CNN "digikey#"
F 6 "RC0805FR-0710KL" H 4650 3000 50  0001 C CNN "manf#"
F 7 "YAGEO" H 4650 3000 50  0001 C CNN "manf"
F 8 "301010361" H 4650 3000 50  0001 C CNN "SeeedOPL#"
F 9 "301010361" H 4650 3000 50  0001 C CNN "SeeedSKU"
F 10 "https://www.digikey.com/en/products/detail/yageo/RC0805FR-0710KL/727535?s=N4IgTCBcDaIMwEYEFoEAYB0aDSBhASgCLb7IByhIAugL5A" H 4650 3000 50  0001 C CNN "PurchaseLink"
F 11 "0805" H 4650 3000 50  0001 C CNN "Package"
F 12 "SMD" H 4650 3000 50  0001 C CNN "Mount Type"
	1    4650 3000
	0    -1   -1   0   
$EndComp
Text Label 4050 3000 0    50   ~ 0
BUTTON_MID
NoConn ~ 1750 1900
$Comp
L Device:D_ALT D4
U 1 1 61680D47
P 4650 2550
AR Path="/61696A39/61680D47" Ref="D4"  Part="1" 
AR Path="/6169E83C/61680D47" Ref="D5"  Part="1" 
AR Path="/6169EA04/61680D47" Ref="D6"  Part="1" 
AR Path="/6169EEAD/61680D47" Ref="D7"  Part="1" 
AR Path="/6169EEB5/61680D47" Ref="D8"  Part="1" 
AR Path="/6169EEBD/61680D47" Ref="D9"  Part="1" 
AR Path="/6169F699/61680D47" Ref="D10"  Part="1" 
AR Path="/6169F6A1/61680D47" Ref="D11"  Part="1" 
AR Path="/6169F6A9/61680D47" Ref="D12"  Part="1" 
F 0 "D4" H 4650 2450 50  0000 C CNN
F 1 "D_ALT" H 4650 2424 50  0001 C CNN
F 2 "Diode_SMD:D_SOD-123" H 4650 2550 50  0001 C CNN
F 3 "https://www.mccsemi.com/pdf/Products/B5817W-B5819W(SOD-123).pdf" H 4650 2550 50  0001 C CNN
F 4 "B5819W" H 4650 2550 50  0001 C CNN "manf#"
F 5 "DIODE SCHOTTKY 40V 1A SOD123" H 4650 2650 50  0000 C CNN "desc"
F 6 "B5819W-TPMSCT-ND" H 4650 2550 50  0001 C CNN "digikey#"
F 7 "Micro Commercial Co" H 4650 2550 50  0001 C CNN "manf"
F 8 "304020034" H 4650 2550 50  0001 C CNN "SeeedSKU"
F 9 "304020034" H 4650 2550 50  0001 C CNN "SeeedOPL#"
F 10 "https://www.digikey.com/en/products/detail/micro-commercial-co/B5819W-TP/3774959?s=N4IgTCBcDaIEIFYAcBGAnAdQLQBUAKAsgMoDCOWAcgCIgC6AvkA" H 4650 2550 50  0001 C CNN "PurchaseLink"
F 11 "SOD-123" H 4650 2550 50  0001 C CNN "Package"
F 12 "SMD" H 4650 2550 50  0001 C CNN "Mount Type"
	1    4650 2550
	-1   0    0    1   
$EndComp
Wire Wire Line
	4500 2550 4000 2550
Connection ~ 4000 2550
Wire Wire Line
	4000 2550 4000 3000
Wire Wire Line
	4800 2550 5500 2550
Wire Wire Line
	5500 2550 5500 3000
$EndSCHEMATC
