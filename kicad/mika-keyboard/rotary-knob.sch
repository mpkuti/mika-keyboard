EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 13
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Device:R R3
U 1 1 61621A9B
P 4500 4150
AR Path="/61620D37/61621A9B" Ref="R3"  Part="1" 
AR Path="/61628A1B/61621A9B" Ref="R9"  Part="1" 
AR Path="/61628BB6/61621A9B" Ref="R15"  Part="1" 
F 0 "R3" H 4570 4196 50  0000 L CNN
F 1 "10k" H 4570 4105 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 4430 4150 50  0001 C CNN
F 3 "https://www.yageo.com/upload/media/product/productsearch/datasheet/rchip/PYu-RC_Group_51_RoHS_L_11.pdf" H 4500 4150 50  0001 C CNN
F 4 "RES 10K OHM 1% 1/8W 0805" H 4500 4150 50  0001 C CNN "desc"
F 5 "311-10.0KCRDKR-ND" H 4500 4150 50  0001 C CNN "digikey#"
F 6 "RC0805FR-0710KL" H 4500 4150 50  0001 C CNN "manf#"
F 7 "YAGEO" H 4500 4150 50  0001 C CNN "manf"
F 8 "301010361" H 4500 4150 50  0001 C CNN "SeeedOPL#"
F 9 "301010361" H 4500 4150 50  0001 C CNN "SeeedSKU"
F 10 "https://www.digikey.com/en/products/detail/yageo/RC0805FR-0710KL/727535?s=N4IgTCBcDaIMwEYEFoEAYB0aDSBhASgCLb7IByhIAugL5A" H 4500 4150 50  0001 C CNN "PurchaseLink"
F 11 "0805" H 4500 4150 50  0001 C CNN "Package"
F 12 "SMD" H 4500 4150 50  0001 C CNN "Mount Type"
	1    4500 4150
	1    0    0    -1  
$EndComp
$Comp
L Device:R R5
U 1 1 616232F7
P 4500 4650
AR Path="/61620D37/616232F7" Ref="R5"  Part="1" 
AR Path="/61628A1B/616232F7" Ref="R11"  Part="1" 
AR Path="/61628BB6/616232F7" Ref="R17"  Part="1" 
F 0 "R5" H 4570 4696 50  0000 L CNN
F 1 "10k" H 4570 4605 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 4430 4650 50  0001 C CNN
F 3 "https://www.yageo.com/upload/media/product/productsearch/datasheet/rchip/PYu-RC_Group_51_RoHS_L_11.pdf" H 4500 4650 50  0001 C CNN
F 4 "RES 10K OHM 1% 1/8W 0805" H 4500 4650 50  0001 C CNN "desc"
F 5 "311-10.0KCRDKR-ND" H 4500 4650 50  0001 C CNN "digikey#"
F 6 "RC0805FR-0710KL" H 4500 4650 50  0001 C CNN "manf#"
F 7 "YAGEO" H 4500 4650 50  0001 C CNN "manf"
F 8 "301010361" H 4500 4650 50  0001 C CNN "SeeedOPL#"
F 9 "301010361" H 4500 4650 50  0001 C CNN "SeeedSKU"
F 10 "https://www.digikey.com/en/products/detail/yageo/RC0805FR-0710KL/727535?s=N4IgTCBcDaIMwEYEFoEAYB0aDSBhASgCLb7IByhIAugL5A" H 4500 4650 50  0001 C CNN "PurchaseLink"
F 11 "0805" H 4500 4650 50  0001 C CNN "Package"
F 12 "SMD" H 4500 4650 50  0001 C CNN "Mount Type"
	1    4500 4650
	1    0    0    -1  
$EndComp
$Comp
L Device:R R4
U 1 1 61623629
P 6450 4150
AR Path="/61620D37/61623629" Ref="R4"  Part="1" 
AR Path="/61628A1B/61623629" Ref="R10"  Part="1" 
AR Path="/61628BB6/61623629" Ref="R16"  Part="1" 
F 0 "R4" H 6520 4196 50  0000 L CNN
F 1 "10k" H 6520 4105 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 6380 4150 50  0001 C CNN
F 3 "https://www.yageo.com/upload/media/product/productsearch/datasheet/rchip/PYu-RC_Group_51_RoHS_L_11.pdf" H 6450 4150 50  0001 C CNN
F 4 "RES 10K OHM 1% 1/8W 0805" H 6450 4150 50  0001 C CNN "desc"
F 5 "311-10.0KCRDKR-ND" H 6450 4150 50  0001 C CNN "digikey#"
F 6 "RC0805FR-0710KL" H 6450 4150 50  0001 C CNN "manf#"
F 7 "YAGEO" H 6450 4150 50  0001 C CNN "manf"
F 8 "301010361" H 6450 4150 50  0001 C CNN "SeeedOPL#"
F 9 "301010361" H 6450 4150 50  0001 C CNN "SeeedSKU"
F 10 "https://www.digikey.com/en/products/detail/yageo/RC0805FR-0710KL/727535?s=N4IgTCBcDaIMwEYEFoEAYB0aDSBhASgCLb7IByhIAugL5A" H 6450 4150 50  0001 C CNN "PurchaseLink"
F 11 "0805" H 6450 4150 50  0001 C CNN "Package"
F 12 "SMD" H 6450 4150 50  0001 C CNN "Mount Type"
	1    6450 4150
	1    0    0    -1  
$EndComp
$Comp
L Device:R R6
U 1 1 61623957
P 6450 4650
AR Path="/61620D37/61623957" Ref="R6"  Part="1" 
AR Path="/61628A1B/61623957" Ref="R12"  Part="1" 
AR Path="/61628BB6/61623957" Ref="R18"  Part="1" 
F 0 "R6" H 6520 4696 50  0000 L CNN
F 1 "10k" H 6520 4605 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 6380 4650 50  0001 C CNN
F 3 "https://www.yageo.com/upload/media/product/productsearch/datasheet/rchip/PYu-RC_Group_51_RoHS_L_11.pdf" H 6450 4650 50  0001 C CNN
F 4 "RES 10K OHM 1% 1/8W 0805" H 6450 4650 50  0001 C CNN "desc"
F 5 "311-10.0KCRDKR-ND" H 6450 4650 50  0001 C CNN "digikey#"
F 6 "RC0805FR-0710KL" H 6450 4650 50  0001 C CNN "manf#"
F 7 "YAGEO" H 6450 4650 50  0001 C CNN "manf"
F 8 "301010361" H 6450 4650 50  0001 C CNN "SeeedOPL#"
F 9 "301010361" H 6450 4650 50  0001 C CNN "SeeedSKU"
F 10 "https://www.digikey.com/en/products/detail/yageo/RC0805FR-0710KL/727535?s=N4IgTCBcDaIMwEYEFoEAYB0aDSBhASgCLb7IByhIAugL5A" H 6450 4650 50  0001 C CNN "PurchaseLink"
F 11 "0805" H 6450 4650 50  0001 C CNN "Package"
F 12 "SMD" H 6450 4650 50  0001 C CNN "Mount Type"
	1    6450 4650
	1    0    0    -1  
$EndComp
Wire Wire Line
	5350 3800 5350 4400
Wire Wire Line
	5350 4400 4500 4400
Wire Wire Line
	4500 4400 4500 4500
Wire Wire Line
	4500 4400 4500 4300
Connection ~ 4500 4400
Wire Wire Line
	5550 4400 6450 4400
Wire Wire Line
	6450 4400 6450 4300
Wire Wire Line
	6450 4500 6450 4400
Connection ~ 6450 4400
Wire Wire Line
	4500 5000 4500 4900
Wire Wire Line
	6450 5000 6450 4900
$Comp
L pspice:CAP C4
U 1 1 61624068
P 4500 5250
AR Path="/61620D37/61624068" Ref="C4"  Part="1" 
AR Path="/61628A1B/61624068" Ref="C7"  Part="1" 
AR Path="/61628BB6/61624068" Ref="C10"  Part="1" 
F 0 "C4" H 4678 5296 50  0000 L CNN
F 1 "0.1u" H 4678 5205 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 4500 5250 50  0001 C CNN
F 3 "https://www.yageo.com/upload/media/product/productsearch/datasheet/mlcc/UPY-GPHC_X7R_6.3V-to-50V_20.pdf" H 4500 5250 50  0001 C CNN
F 4 "CAP CER 0.1UF 50V X7R 0805" H 4500 5250 50  0001 C CNN "desc"
F 5 "311-1140-1-ND" H 4500 5250 50  0001 C CNN "digikey#"
F 6 "CC0805KRX7R9BB104" H 4500 5250 50  0001 C CNN "manf#"
F 7 "YAGEO" H 4500 5250 50  0001 C CNN "manf"
F 8 "302010165" H 4500 5250 50  0001 C CNN "SeeedSKU"
F 9 "302010165" H 4500 5250 50  0001 C CNN "SeeedOPL#"
F 10 "https://www.digikey.com/en/products/detail/yageo/CC0805KRX7R9BB104/302874?s=N4IgTCBcDaIMwEYEFokBYAMrkDkAiIAugL5A" H 4500 5250 50  0001 C CNN "PurchaseLink"
F 11 "0805" H 4500 5250 50  0001 C CNN "Package"
F 12 "SMD" H 4500 5250 50  0001 C CNN "Mount Type"
	1    4500 5250
	1    0    0    -1  
$EndComp
$Comp
L pspice:CAP C5
U 1 1 6162486F
P 6450 5250
AR Path="/61620D37/6162486F" Ref="C5"  Part="1" 
AR Path="/61628A1B/6162486F" Ref="C8"  Part="1" 
AR Path="/61628BB6/6162486F" Ref="C11"  Part="1" 
F 0 "C5" H 6628 5296 50  0000 L CNN
F 1 "0.1u" H 6628 5205 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 6450 5250 50  0001 C CNN
F 3 "https://www.yageo.com/upload/media/product/productsearch/datasheet/mlcc/UPY-GPHC_X7R_6.3V-to-50V_20.pdf" H 6450 5250 50  0001 C CNN
F 4 "CAP CER 0.1UF 50V X7R 0805" H 6450 5250 50  0001 C CNN "desc"
F 5 "311-1140-1-ND" H 6450 5250 50  0001 C CNN "digikey#"
F 6 "CC0805KRX7R9BB104" H 6450 5250 50  0001 C CNN "manf#"
F 7 "YAGEO" H 6450 5250 50  0001 C CNN "manf"
F 8 "302010165" H 6450 5250 50  0001 C CNN "SeeedOPL#"
F 9 "302010165" H 6450 5250 50  0001 C CNN "SeeedSKU"
F 10 "https://www.digikey.com/en/products/detail/yageo/CC0805KRX7R9BB104/302874?s=N4IgTCBcDaIMwEYEFokBYAMrkDkAiIAugL5A" H 6450 5250 50  0001 C CNN "PurchaseLink"
F 11 "0805" H 6450 5250 50  0001 C CNN "Package"
F 12 "SMD" H 6450 5250 50  0001 C CNN "Mount Type"
	1    6450 5250
	1    0    0    -1  
$EndComp
Text Label 6450 5500 3    50   ~ 0
GND
Text Label 4950 1950 2    50   ~ 0
GND
Text Label 5550 2050 0    50   ~ 0
GND
Text HLabel 8750 2200 2    50   Output ~ 0
BUTTON_CAP
Text HLabel 4500 5500 3    50   Input ~ 0
GND
Text HLabel 4500 4000 1    50   Input ~ 0
3V3
Text Label 6450 4000 1    50   ~ 0
3V3
Text HLabel 4400 4900 0    50   Output ~ 0
ROT_A
Wire Wire Line
	4400 4900 4500 4900
Connection ~ 4500 4900
Wire Wire Line
	4500 4900 4500 4800
Text HLabel 6550 4900 2    50   Output ~ 0
ROT_B
Wire Wire Line
	6550 4900 6450 4900
Connection ~ 6450 4900
Wire Wire Line
	6450 4900 6450 4800
Text Notes 4700 1600 0    50   ~ 0
ROTARY KNOB PRESSED: BUTTON_CAP -> GND\nROTARY KNOB NOT PRESSED: BUTTON_CAP -> 3V3
$Comp
L Device:R R1
U 1 1 6167765F
P 6950 1600
AR Path="/61620D37/6167765F" Ref="R1"  Part="1" 
AR Path="/61628A1B/6167765F" Ref="R7"  Part="1" 
AR Path="/61628BB6/6167765F" Ref="R13"  Part="1" 
F 0 "R1" H 7020 1646 50  0000 L CNN
F 1 "10k" H 7020 1555 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 6880 1600 50  0001 C CNN
F 3 "https://www.yageo.com/upload/media/product/productsearch/datasheet/rchip/PYu-RC_Group_51_RoHS_L_11.pdf" H 6950 1600 50  0001 C CNN
F 4 "RES 10K OHM 1% 1/8W 0805" H 6950 1600 50  0001 C CNN "desc"
F 5 "311-10.0KCRDKR-ND" H 6950 1600 50  0001 C CNN "digikey#"
F 6 "RC0805FR-0710KL" H 6950 1600 50  0001 C CNN "manf#"
F 7 "YAGEO" H 6950 1600 50  0001 C CNN "manf"
F 8 "301010361" H 6950 1600 50  0001 C CNN "SeeedOPL#"
F 9 "301010361" H 6950 1600 50  0001 C CNN "SeeedSKU"
F 10 "https://www.digikey.com/en/products/detail/yageo/RC0805FR-0710KL/727535?s=N4IgTCBcDaIMwEYEFoEAYB0aDSBhASgCLb7IByhIAugL5A" H 6950 1600 50  0001 C CNN "PurchaseLink"
F 11 "0805" H 6950 1600 50  0001 C CNN "Package"
F 12 "SMD" H 6950 1600 50  0001 C CNN "Mount Type"
	1    6950 1600
	1    0    0    -1  
$EndComp
$Comp
L Device:R R2
U 1 1 61677665
P 7700 2200
AR Path="/61620D37/61677665" Ref="R2"  Part="1" 
AR Path="/61628A1B/61677665" Ref="R8"  Part="1" 
AR Path="/61628BB6/61677665" Ref="R14"  Part="1" 
F 0 "R2" H 7770 2246 50  0000 L CNN
F 1 "10k" H 7770 2155 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 7630 2200 50  0001 C CNN
F 3 "https://www.yageo.com/upload/media/product/productsearch/datasheet/rchip/PYu-RC_Group_51_RoHS_L_11.pdf" H 7700 2200 50  0001 C CNN
F 4 "RES 10K OHM 1% 1/8W 0805" H 7700 2200 50  0001 C CNN "desc"
F 5 "311-10.0KCRDKR-ND" H 7700 2200 50  0001 C CNN "digikey#"
F 6 "RC0805FR-0710KL" H 7700 2200 50  0001 C CNN "manf#"
F 7 "YAGEO" H 7700 2200 50  0001 C CNN "manf"
F 8 "301010361" H 7700 2200 50  0001 C CNN "SeeedOPL#"
F 9 "301010361" H 7700 2200 50  0001 C CNN "SeeedSKU"
F 10 "https://www.digikey.com/en/products/detail/yageo/RC0805FR-0710KL/727535?s=N4IgTCBcDaIMwEYEFoEAYB0aDSBhASgCLb7IByhIAugL5A" H 7700 2200 50  0001 C CNN "PurchaseLink"
F 11 "0805" H 7700 2200 50  0001 C CNN "Package"
F 12 "SMD" H 7700 2200 50  0001 C CNN "Mount Type"
	1    7700 2200
	0    1    1    0   
$EndComp
$Comp
L pspice:CAP C3
U 1 1 61677670
P 8500 2600
AR Path="/61620D37/61677670" Ref="C3"  Part="1" 
AR Path="/61628A1B/61677670" Ref="C6"  Part="1" 
AR Path="/61628BB6/61677670" Ref="C9"  Part="1" 
F 0 "C3" H 8678 2646 50  0000 L CNN
F 1 "0.56u" H 8678 2555 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 8500 2600 50  0001 C CNN
F 3 "https://www.yageo.com/upload/media/product/productsearch/datasheet/mlcc/UPY-GPHC_X7R_6.3V-to-50V_20.pdf" H 8500 2600 50  0001 C CNN
F 4 "CAP CER 0.56UF 25V X7R 0805" H 8500 2600 50  0001 C CNN "desc"
F 5 "13-CC0805KKX7R8BB564TR-ND" H 8500 2600 50  0001 C CNN "digikey#"
F 6 "CC0805KKX7R8BB564" H 8500 2600 50  0001 C CNN "manf#"
F 7 "YAGEO" H 8500 2600 50  0001 C CNN "manf"
F 8 "https://www.digikey.com/en/products/detail/yageo/CC0805KKX7R8BB564/11492300?s=N4IgTCBcDaIIwGYC0BhFAGAHOgrAaTwA0B2AJUwCEKcA2AFgBVSkA5AERAF0BfIA" H 8500 2600 50  0001 C CNN "PurchaseLink"
F 9 "0805" H 8500 2600 50  0001 C CNN "Package"
F 10 "SMD" H 8500 2600 50  0001 C CNN "Mount Type"
	1    8500 2600
	1    0    0    -1  
$EndComp
Text Label 8500 2850 3    50   ~ 0
GND
Text Label 6950 1450 1    50   ~ 0
3V3
Wire Wire Line
	5550 1850 6950 1850
Connection ~ 6950 1850
Text Label 4850 4400 0    50   ~ 0
A_RAW
Text Label 5850 4400 0    50   ~ 0
B_RAW
Text Label 5800 1850 0    50   ~ 0
BUTTON_PD
$Comp
L Device:D_ALT D?
U 1 1 616AAA55
P 7700 1850
AR Path="/61696A39/616AAA55" Ref="D?"  Part="1" 
AR Path="/6169E83C/616AAA55" Ref="D?"  Part="1" 
AR Path="/6169EA04/616AAA55" Ref="D?"  Part="1" 
AR Path="/6169EEAD/616AAA55" Ref="D?"  Part="1" 
AR Path="/6169EEB5/616AAA55" Ref="D?"  Part="1" 
AR Path="/6169EEBD/616AAA55" Ref="D?"  Part="1" 
AR Path="/6169F699/616AAA55" Ref="D?"  Part="1" 
AR Path="/6169F6A1/616AAA55" Ref="D?"  Part="1" 
AR Path="/6169F6A9/616AAA55" Ref="D?"  Part="1" 
AR Path="/61620D37/616AAA55" Ref="D1"  Part="1" 
AR Path="/61628A1B/616AAA55" Ref="D2"  Part="1" 
AR Path="/61628BB6/616AAA55" Ref="D3"  Part="1" 
F 0 "D1" H 7700 1750 50  0000 C CNN
F 1 "D_ALT" H 7700 1724 50  0001 C CNN
F 2 "Diode_SMD:D_SOD-123" H 7700 1850 50  0001 C CNN
F 3 "https://www.mccsemi.com/pdf/Products/B5817W-B5819W(SOD-123).pdf" H 7700 1850 50  0001 C CNN
F 4 "B5819W" H 7700 1850 50  0001 C CNN "manf#"
F 5 "DIODE SCHOTTKY 40V 1A SOD123" H 7700 1950 50  0000 C CNN "desc"
F 6 "B5819W-TPMSCT-ND" H 7700 1850 50  0001 C CNN "digikey#"
F 7 "Micro Commercial Co" H 7700 1850 50  0001 C CNN "manf"
F 8 "304020034" H 7700 1850 50  0001 C CNN "SeeedOPL#"
F 9 "304020034" H 7700 1850 50  0001 C CNN "SeeedSKU"
F 10 "https://www.digikey.com/en/products/detail/micro-commercial-co/B5819W-TP/3774959?s=N4IgTCBcDaIEIFYAcBGAnAdQLQBUAKAsgMoDCOWAcgCIgC6AvkA" H 7700 1850 50  0001 C CNN "PurchaseLink"
F 11 "SOD-123" H 7700 1850 50  0001 C CNN "Package"
F 12 "SMD" H 7700 1850 50  0001 C CNN "Mount Type"
	1    7700 1850
	-1   0    0    1   
$EndComp
Wire Wire Line
	6950 2200 7550 2200
Wire Wire Line
	6950 1850 6950 2200
Wire Wire Line
	7850 2200 8500 2200
Wire Wire Line
	8500 2200 8500 2350
Wire Wire Line
	8500 2200 8750 2200
Connection ~ 8500 2200
Wire Wire Line
	8500 2200 8500 1850
Wire Wire Line
	8500 1850 7850 1850
Wire Wire Line
	6950 1750 6950 1850
Wire Wire Line
	6950 1850 7550 1850
$Comp
L mika-keyboard:Rotary_Encoder_Switch SW1
U 1 1 616E2AE5
P 5250 1950
AR Path="/61620D37/616E2AE5" Ref="SW1"  Part="1" 
AR Path="/61628A1B/616E2AE5" Ref="SW2"  Part="1" 
AR Path="/61628BB6/616E2AE5" Ref="SW3"  Part="1" 
AR Path="/616E2AE5" Ref="SW?"  Part="1" 
F 0 "SW1" H 5250 2317 50  0000 C CNN
F 1 "Rotary_Encoder_Switch" H 5250 2226 50  0000 C CNN
F 2 "mika-keyboard:XDCR_PEC11R-4215F-S0024" H 5100 2110 50  0001 C CNN
F 3 "https://www.bourns.com/docs/product-datasheets/pec11r.pdf" H 5250 2210 50  0001 C CNN
F 4 "ROTARY ENCODER MECHANICAL 24PPR" H 5250 1950 50  0001 C CNN "desc"
F 5 "PEC11R-4215F-S0024" H 5250 1950 50  0001 C CNN "manf#"
F 6 "PEC11R-4215F-S0024-ND" H 5250 1950 50  0001 C CNN "digikey#"
F 7 "Bourns Inc." H 5250 1950 50  0001 C CNN "manf"
F 8 "https://www.digikey.com/en/products/detail/bourns-inc/PEC11R-4215F-S0024/4499665?s=N4IgTCBcDaIAoFEDCBGFAlAtAFjCgrAGKYDKADGWNpgHIAiIAugL5A" H 5250 1950 50  0001 C CNN "PurchaseLink"
F 9 "thru-hole" H 5250 1950 50  0001 C CNN "Mount Type"
F 10 "cutom" H 5250 1950 50  0001 C CNN "Package"
	1    5250 1950
	1    0    0    -1  
$EndComp
Text Label 4950 1850 2    50   ~ 0
A_RAW
Text Label 4950 2050 2    50   ~ 0
B_RAW
Wire Wire Line
	5350 3800 4400 3800
Wire Wire Line
	4400 3800 4400 1850
Wire Wire Line
	4400 1850 4950 1850
Wire Wire Line
	5550 3300 4950 3300
Wire Wire Line
	4950 3300 4950 2050
Wire Wire Line
	5550 3300 5550 4400
$EndSCHEMATC
