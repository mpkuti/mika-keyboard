# virtualenv -p python3 virtualenv
virtualenv -p python3 circuitpython-virtualenv

# To load your virtual environment type
cd circuitpython-virtualenv
source bin/activate

# To return to the normal environment, type
deactivate
