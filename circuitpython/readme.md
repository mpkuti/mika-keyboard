SERIAL CONSOLE IN LINUX BASH
----------------------------

INSTRUCTIONS: https://learn.adafruit.com/welcome-to-circuitpython/advanced-serial-console-on-linux

ls /dev/ttyACM*

screen /dev/ttyACM0 115200


STUCK AT READ-ONLY FILESYSTEM
-----------------------------

INSTRUCTIONS: https://learn.adafruit.com/welcome-to-circuitpython/troubleshooting

IN REPL:

import storage
storage.erase_filesystem()

After this, reinstall CircuitPython and all the own code needed. The script copy_src_to_board.sh can be used for copying this project to the board.
