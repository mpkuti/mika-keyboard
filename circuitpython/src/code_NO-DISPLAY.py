"""
MIKA-KEYBOARD
"""

import time
import board
import digitalio
import rotaryio
import usb_hid
from keys_dict.keys import key_lookup
from adafruit_hid.consumer_control import ConsumerControl
from adafruit_hid.consumer_control_code import ConsumerControlCode

from adafruit_hid.keyboard import Keyboard
# from adafruit_hid.keyboard_layout_us import KeyboardLayoutUS
from adafruit_hid.keycode import Keycode

import busio
import displayio
import terminalio
import adafruit_displayio_ssd1306
from adafruit_display_text import bitmap_label

START_WITH_APPLICATION = "Desktop"

###############################################################################
# DISPLAY-RELATED CONFIGURATION
LABEL_FONT = terminalio.FONT

WHITE = 0xFFFFFF
BLACK = 0x000000
DISPLAY_WIDTH = 128
DISPLAY_HEIGHT = 32
DISPLAY_CTRL_ADDR = 0x3C  # display controller i2c address
XMID = DISPLAY_WIDTH // 2 - 1
YMID = DISPLAY_HEIGHT // 2 - 1
APPS = ["KiCad SCH", "KiCad LAY", "Inkscape", "Desktop"]
LABEL_STEP = 8  # label animation speed
APP_IDX = 0  # selected application
NEXT_APP_IDX = 0
APPLICATIONS = {}  # text-file-read-test
APPLICATION_ID = 0  # text-file-read-test
###############################################################################

###############################################################################
# KEYBOARD SHORTCUTS FROM TEXT FILE
# open shortcuts.txt file and put contents in application_dict so title and
# shortcut can be looked up for each application
with open("/shortcuts.txt", "r", encoding="utf-8") as filepointer:
    for line in filepointer:
        app, desc, keys = line.split(",")
        combo = []
        for key in keys.split():
            combo.append(key_lookup[key])
        if app in APPLICATIONS:
            APPLICATIONS[app].append((desc.strip(), combo))
        else:
            APPLICATIONS[app] = [(desc.strip(), combo)]
# if desktop is a set of shortcuts in the file then start with desktop as the default
if START_WITH_APPLICATION in APPLICATIONS.keys():
    APPLICATION_ID = START_WITH_APPLICATION
else:
    # if the "start_with_application" is not found,
    # select the first application in the text file
    APPLICATION_ID = list(APPLICATIONS.keys())[0]
APPLICATION_NAMES = []
for i in APPLICATIONS:
    APPLICATION_NAMES.append(i)
print(APPLICATION_NAMES)
print(APPLICATION_ID)
print(APPLICATIONS)
###############################################################################

###################################################
# DISPLAY SETUP
# displayio.release_displays()
# i2c_bus = busio.I2C(scl=board.GP17, sda=board.GP16)
# display_bus = displayio.I2CDisplay(i2c_bus, device_address=DISPLAY_CTRL_ADDR)
# display = adafruit_displayio_ssd1306.SSD1306(
#     display_bus,
#     width=DISPLAY_WIDTH,
#     height=DISPLAY_HEIGHT,
#     auto_refresh=False,
# )
# CREATE TEXT LABELS
text_label = bitmap_label.Label(
    LABEL_FONT,
    text=APPS[APP_IDX],
    color=WHITE,
    scale=1,
    base_alignment=True,
    anchor_point=(0.5, 0.5),  # middle of the screen
    anchored_position=(XMID, YMID),
)
next_text_label = bitmap_label.Label(
    LABEL_FONT,
    text=APPS[NEXT_APP_IDX],
    color=WHITE,
    scale=1,
    base_alignment=True,
    anchor_point=(0.5, 0.5),  # middle of the screen
    anchored_position=(XMID, -YMID),
)
# SHOW THE DISPLAY CONTENT
# labels = displayio.Group()  # create group of display items
# labels.append(text_label)
# labels.append(next_text_label)
# display.show(labels)
label_change_time = time.monotonic()

##################################################
# DISPLAY FUNCTIONS
def select_next_application():
    """
    Slide-in display label of the next application in application list.
    """
    global APP_IDX
    global NEXT_APP_IDX
    global label_change_time
    text_label.scale = 1
    next_text_label.scale = 1
    # SET THE NEXT LABEL TEXT
    if APP_IDX <= (len(APPS) - 2):
        NEXT_APP_IDX = APP_IDX + 1
    else:
        NEXT_APP_IDX = 0
    next_text_label.text = APPS[NEXT_APP_IDX]
    # MOVE NEXT LABEL TO START POSITION
    next_text_label.anchored_position = (XMID, -YMID)
    # ANIMATE
    while next_text_label.anchored_position[1] <= (YMID - LABEL_STEP):
        next_text_label.anchored_position = (
            XMID,
            next_text_label.anchored_position[1] + LABEL_STEP,
        )
        text_label.anchored_position = (
            XMID,
            text_label.anchored_position[1] + LABEL_STEP,
        )
        display.refresh()
    # CHANGE "NEXT_LABEL" TO "SELECTED_LABEL"
    APP_IDX = NEXT_APP_IDX
    text_label.text = APPS[APP_IDX]
    text_label.anchored_position = (XMID, YMID)
    next_text_label.anchored_position = (XMID, -YMID)  # move next_label to off-display
    display.refresh()
    label_change_time = time.monotonic()

def select_next_application_v2():
    """
    Slide-in display label of the next application in application list.
    """
    global APP_IDX
    global NEXT_APP_IDX
    global label_change_time
    text_label.scale = 1
    next_text_label.scale = 1
    # SET THE NEXT LABEL TEXT
    if APP_IDX <= (len(APPLICATION_NAMES) - 2):
        NEXT_APP_IDX = APP_IDX + 1
    else:
        NEXT_APP_IDX = 0
    next_text_label.text = APPLICATION_NAMES[NEXT_APP_IDX]
    # MOVE NEXT LABEL TO START POSITION
    next_text_label.anchored_position = (XMID, -YMID)
    # ANIMATE
    while next_text_label.anchored_position[1] <= (YMID - LABEL_STEP):
        next_text_label.anchored_position = (
            XMID,
            next_text_label.anchored_position[1] + LABEL_STEP,
        )
        text_label.anchored_position = (
            XMID,
            text_label.anchored_position[1] + LABEL_STEP,
        )
        display.refresh()
    # CHANGE "NEXT_LABEL" TO "SELECTED_LABEL"
    APP_IDX = NEXT_APP_IDX
    text_label.text = APPLICATION_NAMES[APP_IDX]
    text_label.anchored_position = (XMID, YMID)
    next_text_label.anchored_position = (XMID, -YMID)  # move next_label to off-display
    display.refresh()
    label_change_time = time.monotonic()

def select_previous_application():
    """
    Slide-in display label of the pravious application in application list.
    """
    global APP_IDX
    global NEXT_APP_IDX
    global label_change_time
    text_label.scale = 1
    next_text_label.scale = 1
    # SET THE NEXT LABEL TEXT
    if APP_IDX > 0:
        NEXT_APP_IDX = APP_IDX - 1
    else:
        NEXT_APP_IDX = len(APPS) - 1
    next_text_label.text = APPS[NEXT_APP_IDX]
    # MOVE NEXT LABEL TO START POSITION
    next_text_label.anchored_position = (XMID, 3*YMID)
    # ANIMATE
    while next_text_label.anchored_position[1] >= (YMID + LABEL_STEP):
        next_text_label.anchored_position = (
            XMID,
            next_text_label.anchored_position[1] - LABEL_STEP,
        )
        text_label.anchored_position = (
            XMID,
            text_label.anchored_position[1] - LABEL_STEP,
        )
        display.refresh()
    # CHANGE "NEXT_LABEL" TO "SELECTED_LABEL"
    APP_IDX = NEXT_APP_IDX
    text_label.text = APPS[APP_IDX]
    text_label.anchored_position = (XMID, YMID)
    next_text_label.anchored_position = (XMID, -YMID)  # move next_label to off-display
    display.refresh()
    label_change_time = time.monotonic()

def select_previous_application_v2():
    """
    Slide-in display label of the pravious application in application list.
    """
    global APP_IDX
    global NEXT_APP_IDX
    global label_change_time
    text_label.scale = 1
    next_text_label.scale = 1
    # SET THE NEXT LABEL TEXT
    if APP_IDX > 0:
        NEXT_APP_IDX = APP_IDX - 1
    else:
        NEXT_APP_IDX = len(APPLICATION_NAMES) - 1
    next_text_label.text = APPLICATION_NAMES[NEXT_APP_IDX]
    # MOVE NEXT LABEL TO START POSITION
    next_text_label.anchored_position = (XMID, 3*YMID)
    # ANIMATE
    while next_text_label.anchored_position[1] >= (YMID + LABEL_STEP):
        next_text_label.anchored_position = (
            XMID,
            next_text_label.anchored_position[1] - LABEL_STEP,
        )
        text_label.anchored_position = (
            XMID,
            text_label.anchored_position[1] - LABEL_STEP,
        )
        display.refresh()
    # CHANGE "NEXT_LABEL" TO "SELECTED_LABEL"
    APP_IDX = NEXT_APP_IDX
    text_label.text = APPLICATION_NAMES[APP_IDX]
    text_label.anchored_position = (XMID, YMID)
    next_text_label.anchored_position = (XMID, -YMID)  # move next_label to off-display
    display.refresh()
    label_change_time = time.monotonic()

def update_label_scale():
    """
    After label text change, this function increases the text size.
    """
    global label_change_time
    if text_label.scale < 2:
        if (time.monotonic() - label_change_time) > 1:
            text_label.scale = 2
            display.refresh()
##################################################

##################################################
# GPIO CONNECTIONS
# ROT1_A_PIN = digitalio.DigitalInOut(board.GP13)
# ROT1_B_PIN = digitalio.DigitalInOut(board.GP14)
# ROT2_A_PIN = digitalio.DigitalInOut(board.GP5)
# ROT2_B_PIN = digitalio.DigitalInOut(board.GP6)
# ROT3_A_PIN = digitalio.DigitalInOut(board.GP1)
# ROT3_B_PIN = digitalio.DigitalInOut(board.GP2)

# CONFIGURE BUTTON PINS
# order:
#  BUTTON1      BUTTON2      BUTTON3
#  BUTTON4      BUTTON5      BUTTON6
#  BUTTON7      BUTTON8      BUTTON9
#  ROT1_BUTTON  ROT2_BUTTON  ROT3_BUTTON
keypress_ports = [
    board.GP11, board.GP9,  board.GP3,
    board.GP20, board.GP8,  board.GP4,
    board.GP15, board.GP21, board.GP0,
    board.GP12, board.GP10, board.GP7,
]
keypress_pins = []

for port in keypress_ports:
    pin = digitalio.DigitalInOut(port)
    pin.direction = digitalio.Direction.INPUT
    # pin.pull = digitalio.Pull.UP  # UP or DOWN
    pin.pull = None  # There are PULL-UP resistors on the PCB
    keypress_pins.append(pin)

# ROT1_A_PIN.direction = digitalio.Direction.INPUT
# ROT1_B_PIN.direction = digitalio.Direction.INPUT
# ROT2_A_PIN.direction = digitalio.Direction.INPUT
# ROT2_B_PIN.direction = digitalio.Direction.INPUT
# ROT3_A_PIN.direction = digitalio.Direction.INPUT
# ROT3_B_PIN.direction = digitalio.Direction.INPUT
# ROT1_A_PIN.pull = digitalio.Pull.DOWN
# ROT1_B_PIN.pull = digitalio.Pull.DOWN
# ROT2_A_PIN.pull = digitalio.Pull.DOWN
# ROT2_B_PIN.pull = digitalio.Pull.DOWN
# ROT3_A_PIN.pull = digitalio.Pull.DOWN
# ROT3_B_PIN.pull = digitalio.Pull.DOWN
###################################################

# CONFIGURE DEBOUNCERS
# ROT0_A_DEBOUNCER = Debouncer(ROT0_A_PIN)
# ROT0_B_DEBOUNCER = Debouncer(ROT0_B_PIN)

###################################################
# ROTARYIO OBJECTS
# ROT0 = rotaryio.IncrementalEncoder(ROT0_A_DEBOUNCER, ROT0_B_DEBOUNCER)
# ROT1 = rotaryio.IncrementalEncoder(ROT1_A_PIN, ROT1_B_PIN)
ROT1 = rotaryio.IncrementalEncoder(board.GP13, board.GP14)
ROT2 = rotaryio.IncrementalEncoder(board.GP5, board.GP6)
ROT3 = rotaryio.IncrementalEncoder(board.GP1, board.GP2)
rot1_last_position = ROT1.position
rot2_last_position = ROT2.position
rot3_last_position = ROT3.position
###################################################

###################################################
# KEYBOARD
keyboard = Keyboard(usb_hid.devices)
# keyboard_layout = KeyboardLayoutUS(keyboard)
###################################################

# CONSUMER CONTROL (volume, brightness etc.)
cc = ConsumerControl(usb_hid.devices)

# led = digitalio.DigitalInOut(board.LED)
# led.direction = digitalio.Direction.OUTPUT


while True:
    # ROT0_A_DEBOUNCER.update()
    # ROT0_B_DEBOUNCER.update()

    # Get rotary encoder positions and the change:
    rot1_position = ROT1.position
    rot2_position = ROT2.position
    rot3_position = ROT3.position

    rot1_change = rot1_position - rot1_last_position
    rot2_change = rot2_position - rot2_last_position
    rot3_change = rot3_position - rot3_last_position

    rot1_last_position = rot1_position
    rot2_last_position = rot2_position
    rot3_last_position = rot3_position

    if rot1_change > 0:
        # ROT1 UP
        print("A up")
        # print("A: " + rot1_position)
        cc.send(ConsumerControlCode.VOLUME_INCREMENT)
    elif rot1_change < 0:
        # ROT1 DOWN
        print("A down")
        # print("A: " + rot1_position)
        cc.send(ConsumerControlCode.VOLUME_DECREMENT)

    if rot2_change > 0:
        # ROT2 UP
        print("B up")
        keyboard.send(ConsumerControlCode.VOLUME_INCREMENT)
    elif rot2_change < 0:
        # ROT2 DOWN
        print("B down")
        keyboard.send(ConsumerControlCode.VOLUME_DECREMENT)

    if rot3_change > 0:
        # ROT3 UP
        print("C up")
        # select_next_application_v2()
        # keyboard.send(ConsumerControlCode.VOLUME_INCREMENT)
    elif rot3_change < 0:
        # ROT3 DOWN
        print("C down")
        # select_previous_application_v2()
        # keyboard.send(ConsumerControlCode.VOLUME_DECREMENT)

    # CATCH THE KEY PRESSES
    for keypress_pin in keypress_pins:
        if not keypress_pin.value:
            # not value -> voltage low -> button IS pressed
            i = keypress_pins.index(keypress_pin)
            while not keypress_pin.value:
                # KEY PRESS: don't do anything
                pass
            # KEY RELEASE: execute command
            key = APPLICATIONS[APPLICATION_ID][i][1]
            keyboard.press(*key)
            # keyboard.release_all()
            keyboard.release(*key)

    # TEST DISPLAY SLIDING
    # time.sleep(1)
    # select_next_application_v2()
    # time.sleep(1)
    # select_next_application_v2()
    # time.sleep(1)
    # select_next_application_v2()
    # time.sleep(1)
    # select_previous_application_v2()
    # time.sleep(1)
    # select_previous_application_v2()
    # time.sleep(2)

    # CALL THIS AT EVERY LOOP update_label_scale() time.sleep(2) #time.sleep(0.01)
    # debounce, do we need it?
