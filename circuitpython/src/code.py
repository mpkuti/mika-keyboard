"""
MIKA-KEYBOARD
"""


# LIBRARIES FROM CircuitPython

import time
import board
import keypad # key press events
import digitalio
import rotaryio
import usb_hid

import busio
import displayio
import terminalio


# LIBRARIES IN DIRECTORY lib/

from adafruit_hid.keyboard import Keyboard
from adafruit_hid.keycode import Keycode
import adafruit_displayio_ssd1306
from adafruit_display_text import bitmap_label

# from adafruit_hid.consumer_control import ConsumerControl
# from adafruit_hid.consumer_control_code import ConsumerControlCode
# from adafruit_hid.keyboard_layout_us import KeyboardLayoutUS


###############################################################################
# SETTINGS
###############################################################################
SHORTCUT_FILE = "./shortcuts.txt"

# If this application does not have entries in the shortcuts.txt file,
# this is replaced by the first application in that file.
#START_WITH_APPLICATION = "KiCad"
START_WITH_APPLICATION = "Numbers"

# DISPLAY-RELATED CONFIGURATION
LABEL_FONT = terminalio.FONT
LABEL_SCALE = 2
LABEL_SCALE_SMALL = 1  # 1 or 2, small label size makes animation faster
LABEL_SCALE_DELAY = 2.5  # seconds to keep scale small after app change
LABEL_STEP = 2  # label animation speed: 1-4

SLEEP_DELAY = 60
APP_CHANGE_MODE_DELAY = 2.0  # seconds, deactivate APP_CHANGE_MODE after

WHITE = 0xFFFFFF
BLACK = 0x000000
DISPLAY_WIDTH = 128
DISPLAY_HEIGHT = 32
DISPLAY_CTRL_ADDR = 0x3C  # display controller i2c address
XMID = DISPLAY_WIDTH // 2 - 1
YMID = DISPLAY_HEIGHT // 2 + 1
APP_IDX = 0  # selected application
NEXT_APP_IDX = 0
APPS = {}  # text-file-read-test
APP_NAME = 0  # text-file-read-test
###############################################################################


###############################################################################
# LOOKUP TABLE FOR KEYBOARD MAPPING
key_lookup = {
    "ESC": Keycode.ESCAPE, "ESCAPE": Keycode.ESCAPE,
    "CTRL": Keycode.CONTROL, "CONTROL": Keycode.CONTROL,
    "SHIFT": Keycode.SHIFT, "SPACE": Keycode.SPACE,
    "TAB": Keycode.TAB, "SUPER": Keycode.WINDOWS, "WINDOWS": Keycode.WINDOWS,
    "ALT": Keycode.ALT, "HOME": Keycode.HOME, "END": Keycode.END,
    "+": Keycode.KEYPAD_PLUS, "-": Keycode.KEYPAD_MINUS, "=": Keycode.EQUALS,
    "F1": Keycode.F1, "F2": Keycode.F2, "F3": Keycode.F3, "F4": Keycode.F4,
    "F5": Keycode.F5, "F6": Keycode.F6, "F7": Keycode.F7, "F8": Keycode.F8,
    "F9": Keycode.F9, "F10": Keycode.F10, "F11": Keycode.F11, "F12": Keycode.F12,
    "1": Keycode.ONE, "2": Keycode.TWO, "3": Keycode.THREE, "4": Keycode.FOUR,
    "5": Keycode.FIVE, "6": Keycode.SIX, "7": Keycode.SEVEN, "8": Keycode.EIGHT,
    "9": Keycode.NINE, "0": Keycode.ZERO,
    "A": Keycode.A, "B": Keycode.B, "C": Keycode.C, "D": Keycode.D, "E": Keycode.E,
    "F": Keycode.F, "G": Keycode.G, "H": Keycode.H, "I": Keycode.I, "J": Keycode.J,
    "K": Keycode.K, "L": Keycode.L, "M": Keycode.M, "N": Keycode.N, "O": Keycode.O,
    "P": Keycode.P, "Q": Keycode.Q, "R": Keycode.R, "S": Keycode.S, "T": Keycode.T,
    "U": Keycode.U, "V": Keycode.V, "W": Keycode.W, "X": Keycode.X, "Y": Keycode.Y,
    "Z": Keycode.Z,
    "Å": Keycode.LEFT_BRACKET, "~": Keycode.RIGHT_BRACKET}
###############################################################################

###############################################################################
# KEYBOARD SHORTCUTS FROM TEXT FILE
# open shortcuts.txt file and put contents in application_dict so title and
# shortcut can be looked up for each application
with open(SHORTCUT_FILE, "r", encoding="utf-8") as filepointer:
    for line in filepointer:
        app, desc, keys = line.split(",")
        combo = []
        for key in keys.split():
            combo.append(key_lookup[key])
        if app in APPS:
            APPS[app].append((desc.strip(), combo))
        else:
            APPS[app] = [(desc.strip(), combo)]

APP_NAMES = []
for i in APPS:
    APP_NAMES.append(i)

# if desktop is a set of shortcuts in the file then start with desktop as the default
if START_WITH_APPLICATION in APPS:
    APP_NAME = START_WITH_APPLICATION
else:
    # if the "start_with_application" is not found,
    # select the first application in the text file
    APP_NAME = list(APPS.keys())[0]
APP_IDX = APP_NAMES.index(APP_NAME)
print("Load shortcuts for: " + str(APP_NAMES))
# print(APP_NAME)
# print(APPS)
# print("index: " + str(APP_IDX))
###############################################################################

###############################################################################
# DISPLAY SETUP
displayio.release_displays()
i2c_bus = busio.I2C(scl=board.GP27, sda=board.GP26, frequency=1000000)
display_bus = displayio.I2CDisplay(i2c_bus, device_address=DISPLAY_CTRL_ADDR)
display = adafruit_displayio_ssd1306.SSD1306(
     display_bus,
     width=DISPLAY_WIDTH,
     height=DISPLAY_HEIGHT,
     auto_refresh=False,
)
# CREATE TEXT LABEL
text_label = bitmap_label.Label(
    LABEL_FONT,
    text=APP_NAME,
    color=WHITE,
    scale=LABEL_SCALE,
    base_alignment=True,
    anchor_point=(0.5, 0.5),  # middle of the screen
    anchored_position=(XMID, YMID),
)
# CREATE ANOTHER TEXT LABEL, WHICH IS HIDDEN AT FIRST
next_text_label = bitmap_label.Label(
    LABEL_FONT,
    text="nextApp", # this should be replaced before this text label is shown
    color=WHITE,
    scale=LABEL_SCALE_SMALL,
    base_alignment=True,
    anchor_point=(0.5, 0.5),  # middle of the screen
    anchored_position=(XMID, -YMID),
)
# SHOW THE DISPLAY CONTENT
labels = displayio.Group()  # create group of display items
labels.append(text_label)
labels.append(next_text_label)
display.show(labels)
label_change_time = time.monotonic()
last_press_time = time.monotonic()
app_change_mode_timer = time.monotonic()

##################################################
# DISPLAY FUNCTIONS

def select_next_application():
    """
    Slide-in display label of the next application in application list.
    """
    global APP_IDX
    global APP_NAME
    global NEXT_APP_IDX
    global label_change_time
    global app_change_mode_timer
    text_label.scale = LABEL_SCALE_SMALL
    next_text_label.scale = LABEL_SCALE_SMALL
    # SET THE NEXT LABEL TEXT
    if APP_IDX <= (len(APP_NAMES) - 2):
        NEXT_APP_IDX = APP_IDX + 1
    else:
        NEXT_APP_IDX = 0
    next_text_label.text = APP_NAMES[NEXT_APP_IDX]
    # MOVE NEXT LABEL TO START POSITION
    next_text_label.anchored_position = (XMID, -YMID)
    # ANIMATE
    while next_text_label.anchored_position[1] <= (YMID - LABEL_STEP):
        next_text_label.anchored_position = (
            XMID,
            next_text_label.anchored_position[1] + LABEL_STEP,
        )
        text_label.anchored_position = (
            XMID,
            text_label.anchored_position[1] + LABEL_STEP,
        )
        display.refresh()
    # CHANGE "NEXT_LABEL" TO "SELECTED_LABEL"
    APP_IDX = NEXT_APP_IDX
    APP_NAME = APP_NAMES[APP_IDX]
    # print("SELECT (" + str(APP_IDX) + "): " + APP_NAME)
    text_label.text = APP_NAMES[APP_IDX]
    text_label.anchored_position = (XMID, YMID)
    next_text_label.anchored_position = (XMID, -YMID)  # move next_label to off-display
    display.refresh()
    label_change_time = time.monotonic()
    app_change_mode_timer = time.monotonic()


def select_previous_application():
    """
    Slide-in display label of the pravious application in application list.
    """
    global APP_IDX
    global APP_NAME
    global NEXT_APP_IDX
    global label_change_time
    global app_change_mode_timer
    text_label.scale = LABEL_SCALE_SMALL
    next_text_label.scale = LABEL_SCALE_SMALL
    # SET THE NEXT LABEL TEXT
    if APP_IDX > 0:
        NEXT_APP_IDX = APP_IDX - 1
    else:
        NEXT_APP_IDX = len(APP_NAMES) - 1
    next_text_label.text = APP_NAMES[NEXT_APP_IDX]
    # MOVE NEXT LABEL TO START POSITION
    next_text_label.anchored_position = (XMID, 3*YMID)
    # ANIMATE
    while next_text_label.anchored_position[1] >= (YMID + LABEL_STEP):
        next_text_label.anchored_position = (
            XMID,
            next_text_label.anchored_position[1] - LABEL_STEP,
        )
        text_label.anchored_position = (
            XMID,
            text_label.anchored_position[1] - LABEL_STEP,
        )
        display.refresh()
    # CHANGE "NEXT_LABEL" TO "SELECTED_LABEL"
    APP_IDX = NEXT_APP_IDX
    APP_NAME = APP_NAMES[APP_IDX]
    # print("SELECT (" + str(APP_IDX) + "): " + APP_NAME)
    # print(APPS[APP_NAME])
    text_label.text = APP_NAMES[APP_IDX]
    text_label.anchored_position = (XMID, YMID)
    next_text_label.anchored_position = (XMID, -YMID)  # move next_label to off-display
    display.refresh()
    label_change_time = time.monotonic()
    app_change_mode_timer = time.monotonic()

def update_label_scale():
    """
    After label text change, this function increases the text size.
    """
    global label_change_time
    global APP_CHANGE_MODE
    if APP_CHANGE_MODE:
        text_label.scale = LABEL_SCALE_SMALL
        display.refresh()
    else:
        text_label.scale = LABEL_SCALE
        display.refresh()

        # if (time.monotonic() - label_change_time) > LABEL_SCALE_DELAY:

            # text_label.scale = LABEL_SCALE_SMALL
            # display.refresh()
    # if text_label.scale < LABEL_SCALE:
        # if (time.monotonic() - label_change_time) > LABEL_SCALE_DELAY:
            # text_label.scale = LABEL_SCALE
            # app_change_mode = False
            # print("EXIT APP CHANGE MODE")
            # display.refresh()

def update_display_brightness():
    """
    After anything is pressed, this activates display for a minute.
    """
    global last_press_time
    if display.is_awake:
        if (time.monotonic() - last_press_time) > SLEEP_DELAY:
            display.sleep()

def update_change_mode():
    """
    After timeout, exit application-change mode
    """
    global APP_CHANGE_MODE
    if APP_CHANGE_MODE:
        if (time.monotonic() - app_change_mode_timer) > APP_CHANGE_MODE_DELAY:
            APP_CHANGE_MODE = False
            print("EXIT APP CHANGE MODE")
###############################################################################


###############################################################################
# GPIO CONNECTIONS

# BUTTONS

# order:
#  BUTTON1      BUTTON2      BUTTON3
#  BUTTON4      BUTTON5      BUTTON6
#  BUTTON7      BUTTON8      BUTTON9
#  ROT1_BUTTON  ROT2_BUTTON  ROT3_BUTTON
KEY_PINS = (
    board.GP11, board.GP9,  board.GP3,
    board.GP20, board.GP8,  board.GP4,
    board.GP15, board.GP21, board.GP0,
    board.GP12, board.GP10, board.GP7,
)
keys = keypad.Keys(
    KEY_PINS,
    value_when_pressed=False,
    pull=False,
    interval=0.01 # 10ms debounce
)

# ROTARY ENCODERS

ROT1 = rotaryio.IncrementalEncoder(board.GP13, board.GP14)
ROT2 = rotaryio.IncrementalEncoder(board.GP5, board.GP6)
ROT3 = rotaryio.IncrementalEncoder(board.GP1, board.GP2)
rot1_last_position = ROT1.position
rot2_last_position = ROT2.position
rot3_last_position = ROT3.position
###############################################################################


###############################################################################
# KEYBOARD OBJECT
keyboard = Keyboard(usb_hid.devices)
# keyboard_layout = KeyboardLayoutUS(keyboard)
###############################################################################


###############################################################################
# CONSUMER CONTROL (volume, brightness etc.)
# cc = ConsumerControl(usb_hid.devices)
###############################################################################

APP_CHANGE_MODE = False

display.refresh()  # show the initial screen


# THE MAIN LOOP STARTS HERE
while True:

    ############################################################
    # Get rotary encoder positions and their change:
    rot1_position = ROT1.position
    rot2_position = ROT2.position
    rot3_position = ROT3.position

    rot1_change = rot1_position - rot1_last_position
    rot2_change = rot2_position - rot2_last_position
    rot3_change = rot3_position - rot3_last_position

    rot1_last_position = rot1_position
    rot2_last_position = rot2_position
    rot3_last_position = rot3_position

    ############################################################
    # ROTATION, SELECT WHICH ACTION TO TAKE
    # ROTATE ENCODER 1
    # for rot_action in rot_actions:
    if rot1_change > 0:
        # ROT1 UP
        display.wake()
        key = APPS[APP_NAME][12][1]
        keyboard.press(*key)
        keyboard.release(*key)
        # cc.send(ConsumerControlCode.VOLUME_INCREMENT)
        last_press_time = time.monotonic()
    elif rot1_change < 0:
        # ROT1 DOWN
        display.wake()
        key = APPS[APP_NAME][13][1]
        keyboard.press(*key)
        keyboard.release(*key)
        # cc.send(ConsumerControlCode.VOLUME_DECREMENT)
        last_press_time = time.monotonic()

    ############################################################
    # ROTATE ENCODER 2
    if rot2_change > 0:
        # ROT2 UP, CLOCKWISE
        display.wake()
        key = APPS[APP_NAME][14][1]
        keyboard.press(*key)
        keyboard.release(*key)
        last_press_time = time.monotonic()
    elif rot2_change < 0:
        # ROT2 DOWN, COUNTER-CLOCKWISE
        display.wake()
        key = APPS[APP_NAME][15][1]
        keyboard.press(*key)
        keyboard.release(*key)
        last_press_time = time.monotonic()

    ############################################################
    # ROTATE ENCODER 3
    # 3RD KNOB CHANGES THE APPLICATION WHEN IN APP_CHANGE_MODE
    if rot3_change > 0:
        # ROT3 UP
        display.wake()
        if APP_CHANGE_MODE:
            select_next_application()
        else:
            key = APPS[APP_NAME][16][1]
            keyboard.press(*key)
            keyboard.release(*key)
        last_press_time = time.monotonic()
    elif rot3_change < 0:
        # ROT3 DOWN
        display.wake()
        if APP_CHANGE_MODE:
            select_previous_application()
        else:
            key = APPS[APP_NAME][17][1]
            keyboard.press(*key)
            keyboard.release(*key)
        last_press_time = time.monotonic()

    ############################################################
    # CATCH THE KEY PRESSES
    key_event = keys.events.get()
    if key_event:
        # KEY WAS PRESSED OR RELEASED
        #print(key_event) # print all key events
        key_number = key_event.key_number

        if key_event.pressed:
            display.wake()  # any button press activates the display
            # KNOB 3 PRESS (key_number 11) TOGGLES APPLICATION CHANGE MODE
            if key_number == 11:
                if APP_CHANGE_MODE:
                    APP_CHANGE_MODE = False
                    print("EXIT APP CHANGE MODE")
                else:
                    APP_CHANGE_MODE = True
                    app_change_mode_timer = time.monotonic()
                    print("ENTER APP CHANGE MODE")
            else:
                print(APPS[APP_NAME][key_number][1])
                keyboard.press(*APPS[APP_NAME][key_number][1])

        if key_event.released:
            keyboard.release(*APPS[APP_NAME][key_number][1])
            last_press_time = time.monotonic()

    update_change_mode()
    update_label_scale()
    update_display_brightness()
