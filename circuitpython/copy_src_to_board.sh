#!/bin/bash

SRC_DIR="./src"
DEST_DIR="/media/mika/CIRCUITPY"

srcFiles=("code.py" "shortcuts.txt")

for srcFile in ${srcFiles[*]}; do
	srcFilePath="${SRC_DIR}/${srcFile}"
	if [[ -f "${srcFilePath}" ]]; then
		echo "COPYING FILE: " "${srcFilePath}"
		cp "${srcFilePath}" "${DEST_DIR}/${srcFile}"
	fi
done

###############################################################################
# lib/
###############################################################################

SRC_DIR="./src/lib"
DEST_DIR="/media/mika/CIRCUITPY/lib"
mkdir --parents ${DEST_DIR}

srcFiles=("adafruit_hid" "adafruit_displayio_ssd1306.mpy" "adafruit_display_text")

for srcFile in ${srcFiles[*]}; do
	srcFilePath="${SRC_DIR}/${srcFile}"
	if [[ -f "${srcFilePath}" ]]; then
		echo "COPYING FILE: " "${srcFilePath}"
		cp "${srcFilePath}" "${DEST_DIR}/${srcFile}"
	fi
	if [[ -d "${srcFilePath}" ]]; then
		echo "COPYING DIRECTORY: " "${srcFilePath}"
		cp -R "${srcFilePath}" "${DEST_DIR}/${srcFile}"
	fi
done
