Code editor Mu-editor needs a certain version of Python. My Linux had newer Python, so I ended up installing older python using Pyenv.


USE
---

# set Python version
pyenv local 3.8.12





INIT (once)
-----------


1. Install and configure Pyenv (https://github.com/pyenv/pyenv)


2. list available Python versions including "3.8"

pyenv install --list | grep 3.8


3. install a Python version

pyenv install -v 3.8.12

