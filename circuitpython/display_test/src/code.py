# BUILT-IN LIBRARIES:
import time
import board
import busio
import displayio
import terminalio

# IN DIRECTORY 'lib':
import adafruit_displayio_ssd1306
from adafruit_display_text import bitmap_label

my_font = terminalio.FONT

WHITE = 0xFFFFFF
BLACK = 0x000000
BORDER = 1

my_text = "I2C Display"

DISPLAY_WIDTH = 128
DISPLAY_HEIGHT = 32
DISPLAY_ROTATION = 0  # must be 0, 90, 180 or 270
DISPLAY_CONTROLLER_ADDRESS = 0x3C

XMID = DISPLAY_WIDTH // 2 - 1
YMID = DISPLAY_HEIGHT // 2 - 1

application_names = ["KiCad SCH", "KiCad LAY", "Inkscape", "Desktop"]
selected_application_index = 0
next_application_index = 1

displayio.release_displays()

i2c = busio.I2C(scl=board.GP17, sda=board.GP16)
display_bus = displayio.I2CDisplay(i2c, device_address=DISPLAY_CONTROLLER_ADDRESS)
display = adafruit_displayio_ssd1306.SSD1306(
    display_bus,
    width=DISPLAY_WIDTH,
    height=DISPLAY_HEIGHT,
    rotation=DISPLAY_ROTATION,
    auto_refresh=False,
)


# Make the display context
labels = displayio.Group()
display.show(labels)

# CREATE TEXT LABELS
selected_text_label = bitmap_label.Label(
    my_font,
    text=application_names[selected_application_index],
    color=WHITE,
    scale=1,
    base_alignment=True,
    anchor_point=(0.5, 0.5),  # middle of the screen
    anchored_position=(XMID, YMID),
)
next_text_label = bitmap_label.Label(
    my_font,
    text=application_names[next_application_index],
    color=WHITE,
    scale=1,
    base_alignment=True,
    anchor_point=(0.5, 0.5),  # middle of the screen
    anchored_position=(XMID, -YMID),
)
labels.append(selected_text_label)
labels.append(next_text_label)


def select_next_application():
    global selected_application_index
    global next_application_index
    # SET THE NEXT LABEL TEXT
    if selected_application_index <= (len(application_names) - 2):
        next_application_index = selected_application_index + 1
    else:
        next_application_index = 0
    next_text_label.text = application_names[next_application_index]
    # MOVE NEXT LABEL TO START POSITION
    next_text_label.anchored_position = (XMID, -YMID)
    # ANIMATE
    while next_text_label.anchored_position[1] < (YMID - 1):
        next_text_label.anchored_position = (
            XMID,
            next_text_label.anchored_position[1] + 3,
        )
        selected_text_label.anchored_position = (
            XMID,
            selected_text_label.anchored_position[1] + 3,
        )
        display.refresh()
        # time.sleep(0.005)
    # CHANGE "NEXT_LABEL" TO "SELECTED_LABEL"
    selected_application_index = next_application_index
    selected_text_label.anchored_position = (XMID, YMID)
    next_text_label.anchored_position = (XMID, -YMID)
    selected_text_label.text = application_names[selected_application_index]
    display.refresh()


def select_previous_application():
    global selected_application_index
    global next_application_index
    # SET THE NEXT LABEL TEXT
    if selected_application_index > 0:
        next_application_index = selected_application_index - 1
    else:
        next_application_index = len(application_names) - 1
    next_text_label.text = application_names[next_application_index]
    # MOVE NEXT LABEL TO START POSITION
    next_text_label.anchored_position = (XMID, 3 * YMID)
    while next_text_label.anchored_position[1] > (YMID + 1):
        next_text_label.anchored_position = (
            XMID,
            next_text_label.anchored_position[1] - 3,
        )
        selected_text_label.anchored_position = (
            XMID,
            selected_text_label.anchored_position[1] - 3,
        )
        display.refresh()
        # time.sleep(0.005)
    # CHANGE "NEXT_LABEL" TO "SELECTED_LABEL"
    selected_application_index = next_application_index
    selected_text_label.anchored_position = (XMID, YMID)
    next_text_label.anchored_position = (XMID, -YMID)
    selected_text_label.text = application_names[selected_application_index]
    display.refresh()


display.refresh()

while True:
    time.sleep(0.5)
    select_next_application()
    time.sleep(0.5)
    select_next_application()
    time.sleep(0.5)
    select_next_application()
    time.sleep(0.5)
    select_previous_application()
    time.sleep(0.5)
    select_previous_application()

