#!/bin/bash

SOURCE_DIR="/media/mika/CIRCUITPY"
DESTINATION_DIR="./src"

srcFiles=("code.py" "shortcuts.txt")

for srcFile in ${srcFiles[*]}; do
	srcFilePath="${SOURCE_DIR}/${srcFile}"
	if [[ -f "${srcFilePath}" ]]; then
		echo "COPYING FILE: " "${srcFilePath}"
		cp --verbose "${srcFilePath}" "${DESTINATION_DIR}/${srcFile}"
	fi
done
